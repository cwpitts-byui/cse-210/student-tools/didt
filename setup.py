""" Setup file for pip install -e support
"""
from setuptools import setup

with open("README.md", "r") as in_file:
    markdown = in_file.read()

with open("requirements.txt", "r") as in_file:
    reqs = [line.strip() for line in in_file.readlines()]

setup(
    name="didt",
    author="Christopher Pitts",
    author_email="christopher.william.pitts@gmail.com",
    url="https://gitlab.com/cwpitts-school/byui/cse-210/student-tools/didt/",
    license="MIT",
    license_files=["LICENSE.md"],
    description="Tool for checking if all modules, classes, and functions have docstrings",
    version="0.1.0",
    long_description=markdown,
    packages=["didt"],
    entry_points={"console_scripts": ["didt=didt.__main__:main"]},
    install_requires=reqs,
)
